const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');
// const solve = require('./CaptchaResolver.js');
const { getMail, getToken, mailUrl, deleteMail } = require('./tempMailGen.js');
const fs = require('fs');

const password = '@vinh123';

const times = 21;
(async () => {
  puppeteer.use(pluginStealth());

  let i = 15;
  while (i < times) {
    i += 1;
    const username = 'testvinh' + i;
    const browser = await puppeteer.launch({
      headless: false
    });

    const pageCityMine = await browser.newPage();
    await pageCityMine.setDefaultNavigationTimeout(0);

    const pageTempMail = await browser.newPage();
    await pageTempMail.setDefaultNavigationTimeout(0);

    await pageCityMine.goto('https://citymine.cc/register', {
      waitUntil: 'domcontentloaded',
    });
    console.log(mailUrl);
    await pageTempMail.goto(mailUrl, {
      waitUntil: 'domcontentloaded',
    });

    const mail = await getMail(pageTempMail);
    console.log('got mail', mail);

    // const mail = 'test inputmail'
    await pageCityMine.bringToFront();
    console.log('reg user');
    await pageCityMine.type('input[name=username]', username);
    await pageCityMine.type('input[name=email]', mail);
    await pageCityMine.type('input[name=email_confirmation]', mail);
    await pageCityMine.type('input[name=password]', password);
    await pageCityMine.type('input[name=password_confirmation]', password);
    await pageCityMine.waitForSelector('.custom-control-label', {timeout: 0});
    // check agreement
    await pageCityMine.$eval('.custom-control-label', (el) => el.click());
    // wait resolve captcha
    console.log('wait for captcha.')
    await pageCityMine.waitForNavigation('domcontentloaded');

    // step mining
    try {
      console.log('adjustment mining');
      await pageCityMine.goto('https://citymine.cc/mining');
      await pageCityMine.waitForSelector('#usdBar');

      const usdBarElement = await pageCityMine.$('#usdBar');
      const usdBarSlider = await usdBarElement.boundingBox();

      await pageCityMine.mouse.click(usdBarSlider.x + usdBarSlider.width - 1, usdBarSlider.y + usdBarSlider.height / 2, {delay: 200});

      await pageCityMine.waitFor(1000);

      await pageCityMine.goto('https://citymine.cc/settings');
    } catch (ex) {
      console.log(ex);
      await pageCityMine.waitForNavigation('domcontentloaded');
    }

    // step request token
    try {
      console.log('request token');
      await pageCityMine.waitForSelector('#requestToken');
      await pageCityMine.$eval('#requestToken', (el) => el.click());
      await pageTempMail.bringToFront();
    } catch (ex2) {
      console.log(ex2);
      await pageTempMail.waitForSelector('#epostalar > ul > li.mail', { timeout: 0});
    }

    let token = '';
    try {
      console.log('get mail token');
      token = await getToken(pageTempMail);
      await pageTempMail.screenshot({path: username + '_'  + token + '.png'});

    } catch (getTokenMailEx) {
      console.log(getTokenMailEx);
      await pageCityMine.waitForSelector('input[name=verify_token]', { timeout: 0});
    }

    if (token !== '') {
      console.log('input token ', token);
      await pageCityMine.bringToFront();
      await pageCityMine.type('input[name=verify_token]', token);
      await pageCityMine.$eval('button[name=savebutton]', (el) => el.click());
      console.log('saved token');
    }

    // delete mail
    await pageTempMail.bringToFront();
    console.log('delete mail');
    await deleteMail(pageTempMail);

    await pageCityMine.bringToFront();

    fs.appendFileSync('file.json', JSON.stringify({ username, password, mail }));
    await browser.close();
  }

})();
