
const mailUrl = 'https://tempail.com/';

const getMail = async (page) => {
    // await page.waitForFunction(() => {
    //     const mail = document.getElementById('mail');
    //     return mail !== 'Chargement.';
    //   });
    await page.waitForSelector('#eposta_adres', {timeout: 0});
    const mail = await page.$eval('#eposta_adres', el => el.value);
    return mail;
}

const getToken = async (pageTempMail) => {
    
      await pageTempMail.waitForSelector('#epostalar > ul > li.mail');

      await pageTempMail.click('#epostalar > ul > li.mail', { delay: 200 });
      await pageTempMail.waitFor(1000);

      // await pageTempMail.screenshot({path: username + i + '_'  + mail + '.png'});
      let frames = await pageTempMail.frames();
      const mailFrame = frames.find((frame) =>
        frame.url().includes('en/api/icerik/')
      );

      const mailContent = await mailFrame.$eval(
        'html > body',
        (el) => el.innerText
      );
      console.log('mailContent', mailContent);

      
      if (mailContent) {
        const words = mailContent.split(' ');
        token = words[43];
      }
      
      console.log('token ', token);
      return token;
}

const deleteMail = async (pageTempMail) => {
  console.log('delete mail');
  await pageTempMail.goto(mailUrl, { waitUntil: 'domcontentloaded' });
  await pageTempMail.$eval('a.yoket-link', (el) => el.click());
}

module.exports = {getMail, getToken, mailUrl, deleteMail};